##Installation

* `brew install postgresql`
* Follow Homebrew's instructions to have postgres run at start
* `gem install bundler foreman`
* `bundle install`
* `bundle exec rake db:create db:migrate`
* `foreman start`
