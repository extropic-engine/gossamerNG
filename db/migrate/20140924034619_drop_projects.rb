class DropProjects < ActiveRecord::Migration
  def up
    remove_reference :events, :project
    drop_table :projects
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
