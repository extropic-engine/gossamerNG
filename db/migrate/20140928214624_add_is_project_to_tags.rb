class AddIsProjectToTags < ActiveRecord::Migration
  def change
    add_column :tags, :is_project, :boolean
  end
end
