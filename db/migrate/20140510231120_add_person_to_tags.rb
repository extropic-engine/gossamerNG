class AddPersonToTags < ActiveRecord::Migration
  def change
    add_reference :tags, :person, index: true
  end
end
