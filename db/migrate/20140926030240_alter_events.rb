class AlterEvents < ActiveRecord::Migration
  def up
    remove_column :events, :start
    remove_column :events, :end
    add_column :events, :date, :date
    add_column :events, :hours, :integer
  end

  def down
    remove_column :events, :date
    remove_column :events, :hours
    add_column :events, :start, :datetime
    add_column :events, :end, :datetime
  end
end
