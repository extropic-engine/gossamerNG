class ChangeTagArchivedToNotNull < ActiveRecord::Migration
  def up
    change_column :tags, :archived, :boolean, null: false
  end

  def down
    change_column :tags, :archived, :boolean, null: true
  end
end
