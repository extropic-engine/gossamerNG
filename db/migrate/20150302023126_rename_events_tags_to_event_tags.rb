class RenameEventsTagsToEventTags < ActiveRecord::Migration
  def change
    rename_table :events_tags, :event_tags
  end
end
