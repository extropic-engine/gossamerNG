class CreateTagGroups < ActiveRecord::Migration
  def change
    create_table :tag_groups do |t|
      t.string :name
      t.text :description
      t.references :person, index: true

      t.timestamps
    end
    add_reference :tags, :tag_group, index: true
  end
end
