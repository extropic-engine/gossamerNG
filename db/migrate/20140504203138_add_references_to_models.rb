class AddReferencesToModels < ActiveRecord::Migration
  def change
    add_reference :events, :person, index: true
    add_reference :events, :project, index: true
    create_join_table :events, :tags
    add_reference :projects, :person, index: true
  end
end
