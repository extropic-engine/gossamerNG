class BalanceController < ApplicationController
  before_action :authenticate_person!

  def send
    # TODO: look these up in the database & track amounts there
    params[:sender]
    params[:receiver]

    @result = {
      :errors => false,
      :balance => params[:amount]
    }
  end

  def settle
    # TODO: look these up in the database & track amounts there
    params[:sender]
    params[:receiver]

    @result = {
        :errors => false,
        :balance => params[:amount]
    }
  end
end