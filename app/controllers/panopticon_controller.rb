class PanopticonController < ApplicationController
  before_action :authenticate_person!

  def index
    @tags = Tag.joins('LEFT JOIN tag_groups tg ON tags.tag_group_id=tg.id LEFT JOIN events_tags ON tags.id=events_tags.tag_id')
               .select('tags.*, tg.name AS tag_group_name, tg.description AS tag_group_description, COUNT(events_tags.event_id) AS event_count')
               .group('tags.id, tg.id')
               .where(person_id: current_person.id)
  end
end
