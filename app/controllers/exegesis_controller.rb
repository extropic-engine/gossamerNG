class ExegesisController < ApplicationController
  before_action :authenticate_person!

  def index
    @month_names = %w(January February March April May June July August September October November December)

    @exegesis = {}
    # TODO: update this to work with tags instead of projects
    events = Event.joins('LEFT JOIN event_tags ON events.id=event_tags.event_id LEFT JOIN tags ON event_tags.tag_id=tags.id')
                  .where(person_id: current_person.id)
                  .select('SUM(hours) AS hours,
                           EXTRACT(MONTH FROM "date") AS "month",
                           EXTRACT(YEAR FROM "date") AS "year",
                           tags.id,
                           tags.name')
                  .group('"year", "month", tags.id')
    # There has got to be a more efficient way to do this loop.
    events.each do |event|
      year = event.year.to_i
      month = event.month.to_i
      @exegesis[year] = {} unless @exegesis.has_key? year
      @exegesis[year][month] = {} unless @exegesis[year].has_key? month
      @exegesis[year][month][event.id] = { name: event.name, hours: event.hours.to_i }
    end
  end
end
