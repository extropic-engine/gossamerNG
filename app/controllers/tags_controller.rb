class TagsController < ApplicationController
  before_action :authenticate_person!
  before_action :set_tag, only: [:show, :edit, :update, :destroy]

  # POST /tags.json
  def create
    @tag = Tag.new(tag_params)
    @tag.archived = tag_params[:archived] || false
    @tag.person = current_person

    respond_to do |format|
      if @tag.save
        format.json { render :show, status: :created, location: @tag }
      else
        format.json { render json: @tag.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      @tag = Tag.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tag_params
      params.require(:tag).permit(:name, :archived)
    end
end
