class EventsController < ApplicationController
  before_action :authenticate_person!
  before_action :set_event, only: [:show]

  # GET /events.json
  def index
    @events = Event.all.where(person_id: current_person.id).includes(:tags)
  end

  # POST /events.json
  def create
    @event = Event.new(event_params.merge({ person_id: current_person.id }))
    @event.tags = Tag.find(params[:tags])

    respond_to do |format|
      # TODO: For some reason, @event.save blows up here if any tags were passed in.
      if @event.save
        format.html { redirect_to @event, notice: 'Event successfully saved.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:date, :hours, :tags)
    end
end
