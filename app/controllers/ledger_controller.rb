class LedgerController < ApplicationController
  before_action :authenticate_person!

  def index
    @tags = Tag.all.where(person_id: current_person.id)
    @events = Event.all.where(person_id: current_person.id).includes(:tags).order('date DESC')
  end
end
