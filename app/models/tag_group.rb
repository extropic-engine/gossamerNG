class TagGroup < ActiveRecord::Base
  belongs_to :person
  has_many :tags
end
