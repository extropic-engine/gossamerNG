class Event < ActiveRecord::Base
  belongs_to :person
  has_many :event_tags
  has_many :tags, through: :event_tags

  def to_s
    "Event \##{self.id}: Worked on #{self.tags.length} tags for #{self.hours} hours on #{self.date}"
  end
end
