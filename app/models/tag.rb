class Tag < ActiveRecord::Base
  belongs_to :person
  belongs_to :tag_group
  has_many :event_tags
  has_many :events, through: :event_tags
  validates :name, presence: true
end
