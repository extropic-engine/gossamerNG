json.array!(@events) do |event|
  json.extract! event, :id, :date, :hours
  json.url event_url(event, format: :json)
end
