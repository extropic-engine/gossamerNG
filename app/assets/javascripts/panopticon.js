Gossamer.controller('panopticon', ['$scope', '$http', function($scope, $http) {
  $scope.add_tag = function() {
    var name = prompt('Tag name:');
    if (name == null) {
      return;
    }
    $http.post('tags.json', { name: name }).success(function(data) {
      // TODO: update current view with returned data
      alert(JSON.stringify(data));
    }).error(function(data) {
      alert(data);
    });
  };
  // TODO: add ability to update & archive tags
}]);
