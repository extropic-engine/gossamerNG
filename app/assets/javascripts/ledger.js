Gossamer.controller('ledger', ['$scope', '$http', function($scope, $http) {
  $scope.event = {
    tags: [],
    hours: '',
    date: ''
  };
  $scope.add_event = function() {
    $http.post('events.json', $scope.event).success(function(data) {
      // TODO: update current view with returned data
      alert(JSON.stringify(data));
    }).error(function(data) {
      alert(data);
    });
  };
}]);
